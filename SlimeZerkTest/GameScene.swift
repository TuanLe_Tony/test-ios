//
//  GameScene.swift
//  SlimeZerkTest
//
//  Created by Parrot on 2019-02-25.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var xn:CGFloat = 0
    var yn:CGFloat = 0
    
    
    // MARK: Variables for tracking time
    private var lastUpdateTime : TimeInterval = 0
    
    //Add Evil Otta
    var evilOtto:SKSpriteNode = SKSpriteNode()
    
    var walls:SKSpriteNode = SKSpriteNode()
    var walls1:SKSpriteNode = SKSpriteNode()
    var walls2:SKSpriteNode = SKSpriteNode()
    var walls3:SKSpriteNode = SKSpriteNode()
    // MARK: Sprite variables
    var enemy:SKSpriteNode = SKSpriteNode()
    var enemy1:SKSpriteNode = SKSpriteNode()
    var player:SKSpriteNode = SKSpriteNode()
    var upButton:SKSpriteNode = SKSpriteNode()
    var downButton:SKSpriteNode = SKSpriteNode()
    var leftButton:SKSpriteNode = SKSpriteNode()
    var rightButton:SKSpriteNode = SKSpriteNode()
    
    var bButton:SKSpriteNode = SKSpriteNode()
    
    var bullet:SKSpriteNode = SKSpriteNode()
    
    var musicButton:SKSpriteNode = SKSpriteNode()
    
    var gate:SKSpriteNode = SKSpriteNode()
    // MARK: Label variables
    var livesLabel:SKLabelNode = SKLabelNode(text:"")
    var pointsLabel:SKLabelNode = SKLabelNode(text:"")
    
    var lives = 2
    var points = 0
    // MARK: Scoring and Lives variables
    
    
    // MARK: Game state variables
    
    
    
    let backgroundSound = SKAudioNode(fileNamed: "halloween.wav")
//    let backgroundSound = SKAction.playSoundFileNamed("halloween.wav", waitForCompletion: false)
    // MARK: Default GameScene functions
    // -------------------------------------
    
    var isEvilSpawn = false
    override func didMove(to view: SKView) {
        // add a boundary around the scene
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.name = "wall"
        // initialize delegate
        self.physicsWorld.contactDelegate = self
        
        
         // -----------------  R2 ----------------//
        //end
        self.addChild(backgroundSound)
        
        
        self.lastUpdateTime = 0
        
        //get music
        self.musicButton = self.childNode(withName: "musicButton") as! SKSpriteNode
        
        
         self.bButton = self.childNode(withName: "bButton") as! SKSpriteNode
        
        self.walls = self.childNode(withName: "walls") as! SKSpriteNode
        //hitbox
        self.walls.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.walls.frame.width, height:self.walls.frame.height))
        self.walls.physicsBody?.affectedByGravity = false
        self.walls.physicsBody?.isDynamic = false
        //end hitbox
        self.walls1 = self.childNode(withName: "walls1") as! SKSpriteNode
        //hitbox
        self.walls1.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.walls1.frame.width, height:self.walls1.frame.height))
        self.walls1.physicsBody?.affectedByGravity = false
        self.walls1.physicsBody?.isDynamic = false
        //end hitbox
        
        self.walls2 = self.childNode(withName: "walls2") as! SKSpriteNode
        //hitbox
        self.walls2.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.walls2.frame.width, height:self.walls2.frame.height))
        self.walls2.physicsBody?.affectedByGravity = false
        self.walls2.physicsBody?.isDynamic = false
        //end hitbox
        self.walls3 = self.childNode(withName: "walls3") as! SKSpriteNode
        //hitbox
        self.walls3.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.walls3.frame.width, height:self.walls3.frame.height))
        self.walls3.physicsBody?.affectedByGravity = false
        self.walls3.physicsBody?.isDynamic = false
        //end hitbox
        
        // get sprites from Scene Editor
        self.player = self.childNode(withName: "player") as! SKSpriteNode
        
        //hitbox
        self.player.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.player.frame.width, height:self.player.frame.height))
        self.player.physicsBody?.affectedByGravity = false
        self.player.physicsBody?.isDynamic = true
    
        
        //end hitbox
        self.enemy = self.childNode(withName: "enemy") as! SKSpriteNode
        self.enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.enemy.frame.width, height:self.enemy.frame.height))
        self.enemy.physicsBody?.affectedByGravity = false
        self.enemy.physicsBody?.isDynamic = true
        
        //end hitbox
        self.enemy1 = self.childNode(withName: "enemy1") as! SKSpriteNode
        self.enemy1.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.enemy1.frame.width, height:self.enemy1.frame.height))
        self.enemy1.physicsBody?.affectedByGravity = false
        self.enemy1.physicsBody?.isDynamic = true
        
        self.upButton = self.childNode(withName: "upButton") as! SKSpriteNode
        self.downButton = self.childNode(withName: "downButton") as! SKSpriteNode
        self.leftButton = self.childNode(withName: "leftButton") as! SKSpriteNode
        self.rightButton = self.childNode(withName: "rightButton") as! SKSpriteNode
        
        self.gate = self.childNode(withName: "gate") as! SKSpriteNode
        //hitbox
        self.gate.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.gate.frame.width, height:self.gate.frame.height))
        
        
        self.gate.physicsBody?.affectedByGravity = false
        self.gate.physicsBody?.isDynamic = false
        
        
        //end hitbox
        
        self.physicsWorld.contactDelegate = self
        
        // get labels from Scene Editor
        self.livesLabel = self.childNode(withName: "livesLabel") as! SKLabelNode
        self.pointsLabel = self.childNode(withName: "scoreLabel") as! SKLabelNode
        
        
        // get sprites from Scene Editor
        self.bullet = self.childNode(withName: "bullet") as! SKSpriteNode
        
        //hitbox
        self.bullet.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.bullet.frame.width, height:self.bullet.frame.height))
        self.bullet.physicsBody?.affectedByGravity = false
        self.bullet.physicsBody?.isDynamic = true
        
        
        
        // get evil otta
     
        self.evilOtto = self.childNode(withName: "evilOtto") as! SKSpriteNode
        
       
        //hitbox
        self.evilOtto.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width:self.evilOtto.frame.width, height:self.evilOtto.frame.height))
        self.evilOtto.physicsBody?.affectedByGravity = false
        self.evilOtto.physicsBody?.isDynamic = false
        
        
        
    }
    var isMusicOn = true
    
    var isShootingRight = false
    var isShootingLeft = false
    
    // -----------------  R1 ----------------//
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let pointTouched = touch.location(in: self)
            
            if upButton.contains(pointTouched) {
                player.position.y += 30
                bullet.position.y += 30
            }
            if downButton.contains(pointTouched) {
                player.position.y -= 30
                bullet.position.y -= 30
            }
            if leftButton.contains(pointTouched) {
                player.position.x -= 30
                self.player.xScale = -0.6 // -----------------  R11 ----------------//
                
                bullet.position.x -= 30 // Bullet always move with player
                
                 //if face turn left the bullet will shoot left
                isShootingRight = false
                isShootingLeft = true
                //
                self.bullet.position = CGPoint(x: self.size.width/2 * 4, y:self.size.height/2 * 4)// reset bullet out of screen
                
            }
            if rightButton.contains(pointTouched) {
                self.player.xScale = 0.6 // -----------------  R11 ----------------//
                player.position.x += 30
                
                bullet.position.x += 30 // Bullet always move with player
                
                //if face turn right the bullet will shoot right
                isShootingRight = true
                isShootingLeft = false
                //
                self.bullet.position = CGPoint(x: self.size.width/2 * 4, y:self.size.height/2 * 4) // reset bullet out of screen
            }
            
            //clicked music   // -----------------  R6 & R7 ----------------//
            
             // -----------------  R13 ----------------//
            if musicButton.contains(pointTouched) {
                //play sound
//                run(backgroundSound)
                if (isMusicOn == true){
                backgroundSound.run(SKAction.stop())
                   
                }
            }
            
            //shoot button
            if bButton.contains(pointTouched) {
//                isShootingRight = true
//                isShootingLeft = true
                self.bullet.position = CGPoint(x: self.player.position.x, y:self.player.position.y)
                
            }
            
        }
    }
    
    
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//
//    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
       
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    @objc func restartGame() {
        // reset Level 1
        let scene = GameScene(fileNamed:"BerzerkLevel1")
        scene!.scaleMode = .aspectFill
        view!.presentScene(scene)
    }
    // -----------------  R9 ----------------//
    @objc func Win() {
        // load Level 2   (requirement just has 2 levels , so this is a cheating way & the fast way :))
        let scene = GameScene(fileNamed:"BerzerkLevel2")
        scene!.scaleMode = .aspectFill
        view!.presentScene(scene)
    }
    override func update(_ currentTime: TimeInterval) {
        
        // -----------------  R8 ----------------// -----------------  R12----------------////
        // The Math was used from android
        let a = self.player.position.x - self.enemy1.position.x
        let b = self.player.position.y - self.enemy1.position.y
        let distanceEnemy1 = sqrt((a*a) + (b*b))
        self.xn = a/distanceEnemy1
        self.yn = b/distanceEnemy1
        
        self.enemy1.run(SKAction.moveBy(x: xn, y: yn, duration: 0.3))
        //another enemy
        let c = self.player.position.x - self.enemy.position.x
        let d = self.player.position.y - self.enemy.position.y
        let distanceEnemy = sqrt((c*c) + (d*d))
        self.xn = c/distanceEnemy
        self.yn = d/distanceEnemy
        
        self.enemy.run(SKAction.moveBy(x: xn, y: yn, duration: 0.3))
        
        
        //--------------------- END ----------------
        
        if (isShootingRight == true){
            self.bullet.position.x = self.bullet.position.x + 100
        }
        if (isShootingLeft == true){
            self.bullet.position.x = self.bullet.position.x - 100
        }
        
        //check if bullet hits the walls
        // -----------------  R14 ----------------//
        if (self.bullet.frame.intersects(self.walls.frame) ||
            self.bullet.frame.intersects(self.walls1.frame) ||
            self.bullet.frame.intersects(self.walls2.frame) ||
            self.bullet.frame.intersects(self.walls3.frame)){
            print("bullet touched the walls")
            //instead of removeChildFrom Parent I reset bullet position out of screen
            self.bullet.position = CGPoint(x: self.size.width/2 * 4, y:self.size.height/2 * 4)
        }
        
         // -----------------  R3 ----------------//
        if (self.player.frame.intersects(self.gate.frame)){
            print("touched")
            self.Win()
        }
        // COLLISION PLAYER & ENEMY   // -----------------  R4 ----------------// 
        if (self.player.frame.intersects(self.enemy.frame)){
            print("player touched enemy")
            
//            self.enemy.removeFromParent()
            self.enemy.position = CGPoint(x: self.size.width/2 * 2 + 500 , y: 0) // move the enemy out of the screenn
            self.enemy.removeFromParent()
            self.lives = lives - 1
            self.livesLabel.text = "\(self.lives)"
            
        }
        if (self.player.frame.intersects(self.enemy1.frame)){
            print("player touched enemy")
            
            //self.enemy.removeFromParent()
            self.enemy1.position = CGPoint(x: self.size.width/2 * 2 + 500 , y: 0) // move the enemy out of the screenn
            self.enemy1.removeFromParent()
            self.lives = lives - 1
            self.livesLabel.text = "\(self.lives)"
            
        }
        // COLLISION BULLLET & ENEMY
        // -----------------  R15 & R16----------------//
        if (self.bullet.frame.intersects(self.enemy.frame)){
            print("player touched enemy")
            
            //            self.enemy.removeFromParent()
            self.enemy.position = CGPoint(x: self.size.width/2 * 2 + 500 , y: 0) // move the enemy out of the screenn
            self.enemy.removeFromParent()// and remove it
            
            //instead of removeChildFrom Parent I reset bullet position out of screen
            self.bullet.position = CGPoint(x: self.size.width/2 * 4, y:self.size.height/2 * 4)
            
            self.points = points + 50
            self.pointsLabel.text = "\(self.points)"
            
        }
        if (self.bullet.frame.intersects(self.enemy1.frame)){
            print("player touched enemy")
            
            //            self.enemy.removeFromParent()
            self.enemy1.position = CGPoint(x: self.size.width/2 * 2 + 500 , y: 0) // move the enemy out of the screenn
            self.enemy1.removeFromParent()// and remove it
            
            //instead of removeChildFrom Parent I reset bullet position out of screen
            self.bullet.position = CGPoint(x: self.size.width/2 * 4, y:self.size.height/2 * 4)
            
            self.points = points + 50
            self.pointsLabel.text = "\(self.points)"
            
        }
    
        // Called before each frame is rendered
        
        // Initialize _lastUpdateTime if it has not already been
        if (self.lastUpdateTime == 0) {
            self.lastUpdateTime = currentTime
        }

        // Calculate time since last update
        let dt = currentTime - self.lastUpdateTime
    
        // HINT: This code prints "Hello world" every 5 seconds
        
        //----------------------- R17 --------------//
        if (dt >= 30) { // AFter 30s Spawn Evil Otto
            print("30s")
            
            isEvilSpawn = true
        }
        if (isEvilSpawn == true){
            let e = self.player.position.x - self.evilOtto.position.x
            let f = self.player.position.y - self.evilOtto.position.y
            let distanceEvil = sqrt((e*e) + (f*f))
            self.xn = e/distanceEvil
            self.yn = f/distanceEvil
            
            self.evilOtto.run(SKAction.moveBy(x: xn, y: yn, duration: 0.3))
            
            self.lastUpdateTime = currentTime
        }
        
       
        
         // -----------------  R5 ----------------//
        if (self.lives == 0){
            let message = SKLabelNode(text:"GAME OVER!")
            message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
            message.fontColor = UIColor.magenta
            message.fontSize = 100
            message.fontName = "AvenirNext-Bold"
            
            addChild(message)
            self.lastUpdateTime = currentTime
            if (dt > 2 ) {
                self.lastUpdateTime = currentTime
                print("GAME OVER")
                self.restartGame()
            }
            // THE EVIL OTTO Spawns
            
        }
        if (self.evilOtto.frame.intersects(self.player.frame)){
            let message = SKLabelNode(text:"GAME OVER!")
            message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
            message.fontColor = UIColor.magenta
            message.fontSize = 100
            message.fontName = "AvenirNext-Bold"
            
            addChild(message)
            self.restartGame()
            
        }
       
        
    }
    
    // MARK: Custom GameScene Functions
    // Your custom functions can go here
    // -------------------------------------
    
    
}
